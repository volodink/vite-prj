import { describe, it, expect } from 'vitest';
import getMaxContainerVolume from './container';


describe('Объем контейнера должен вычисляться корректно', async () => {
    it('1,1 ==> 1', () => {
        expect(getMaxContainerVolume([1,1])).toBe(1);
    });
    it('1,2,1 ==> 2', () => {
        expect(getMaxContainerVolume([1,2,1])).toBe(2);
    });
    it('4,3,2,1,4 ==> 16', () => {
        expect(getMaxContainerVolume([4,3,2,1,4])).toBe(16);
    });
    it('1,8,6,2,5,4,8,3,7 ==> 49', () => {
        expect(getMaxContainerVolume([1,8,6,2,5,4,8,3,7])).toBe(49);
    });
});
