const getMaxContainerVolume = (heights: number[]): number => {
  let maxVolume = 0;
  let left = 0;
  let right = heights.length - 1;

  while (left < right) {
    const width = right - left;
    const minHeight = Math.min(heights[left], heights[right]);
    const volume = width * minHeight;
    maxVolume = Math.max(maxVolume, volume);

    if (heights[left] < heights[right]) {
      left++;
    } else {
      right--;
    }
  }

  return maxVolume;
};

export default getMaxContainerVolume;