import getMaxContainerVolume from "./utils/container"; 

const calculateContainerVolume = () => {
  const inputElement = document.getElementById("input") as HTMLInputElement;
  const outputElement = document.getElementById("output") as HTMLDivElement;

  const input = inputElement.value
    .split(",")
    .map((num) => parseInt(num.trim()));
  const maxVolume = getMaxContainerVolume(input);
  outputElement.textContent = maxVolume.toString();
};


document.getElementById("btn_calculate")?.addEventListener('click', calculateContainerVolume);
